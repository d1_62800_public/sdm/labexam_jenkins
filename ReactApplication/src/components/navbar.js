import {Link} from 'react-router-dom'
const Navbar=()=>{
    return (
    
          <nav className="navbar navbar-expand-lg bg-primary" >
  <div className="container-fluid" >
    <Link className="navbar-brand" to="/">CARAPI</Link>
    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
    <div className="collapse navbar-collapse" id="navbarNav">
      <ul className="navbar-nav">
        <li className="nav-item">
          <Link className="nav-link active" aria-current="page" to="/home">Home</Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link active" to="/signin">Signin</Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link active" to="/addcar">Add Car</Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link active" to="/deletecar">Delete Car</Link>
        </li>
      </ul>
    </div>
  </div>
</nav>
      
    )

}
export default Navbar;