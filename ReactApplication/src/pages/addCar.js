import { useState } from 'react'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'
const AddCar = () => {
  const [name, setName] = useState('')
  const [model, setModel] = useState('')
  const [price, setPrice] = useState('')
  const [carColor, setCarColor] = useState('')
  const Navigate = useNavigate()
  const addcar = () => {
    if (
      name.length === 0 ||
      model.length === 0 ||
      price.length === 0 ||
      carImage === 0
    )
      alert('please fill all the field')
    else {
      axios
        .post('http://localhost/car/addcar', {
          name,
          model,
          price,
          carImage,
        })
        .then((response) => {
          const result = response.data
          if (result['status'] === 'error') {
            alert('something goes wrong please try again')
          } else {
            alert('Car Added Successfully.....')

             Navigate('/home');
          }
        })
    }
  }
  
  function handlechage(e) {
      setCarImage(Path2D.get)
  }
  return (
    <div className="row">
      <div className="col"></div>
      <div className="col">
        <form>
          <div className="mb-3">
            <label htmlFor="exampleInputEmail1" className="form-label">
              Name
            </label>
            <input
              onChange={(event) => {
                setName(event.target.value)
              }}
              type="text"
              className="form-control"
              id="exampleInputEmail1"
              aria-describedby="emailHelp"
            />
          </div>
          <div className="mb-3">
            <label htmlFor="exampleInputPassword1" className="form-label">
              Model
            </label>
            <input
              onChange={(event) => {
                setModel(event.target.value)
              }}
              type="text"
              className="form-control"
              id="exampleInputPassword1"
            />
          </div>
          <div className="mb-3">
            <label htmlFor="exampleInputEmail1" className="form-label">
              Price
            </label>
            <input
              onChange={(event) => {
                setPrice(event.target.value)
              }}
              type="number"
              className="form-control"
              id="exampleInputEmail1"
              aria-describedby="emailHelp"
            />
          </div>
          <div className="mb-3">
            <label htmlFor="formFile" className="form-label">
              Car Image
            </label>
            <div><input type="file"className='form-control' onChange={handlechage} /></div>
            {/* <input
              onChange={(event) => {
                setCarImage(event.target.value)
              }}
              className="form-control"
              type="file"
              id="formFile"
            /> */}
          </div>

          <button onClick={addcar} type="submit" className="btn btn-primary">
            Submit
          </button>
        </form>
      </div>
      <div className="col"></div>
    </div>
  )
}
export default AddCar
