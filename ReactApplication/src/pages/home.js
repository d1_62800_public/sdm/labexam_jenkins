// import {Link} from 'react-router-dom'
import axios from 'axios';
 import { useState } from 'react';
//  import deletecar from './deletecar';
const Home=()=>{
    let [cars,setCars]=useState([]);

    function onLoadHome(){
    axios.get('http://localhost/car/getallcar').then((response)=>{
      setCars(response.data.data);
      }).catch((error)=>{
           alert(error);
      })
      console.log(cars);

}

      return(
         
      <div className="container" onLoad={onLoadHome}>
          
          <button onClick={onLoadHome}>Load me </button>
     
      <div style={{}} className='row'>
        {cars.map((listing) => {
          return (
            <div
              className='card col'
              style={{
                margin: 20,
                display: 'inline-block',
                cursor: 'pointer',
              }}>
              <img  src={listing.carImage} alt="Currentlty not available" />
              <div className='card-body'>
                <h5 className='card-title'>{listing.name}</h5>
                <h6 className='card-text'>{listing.model}</h6>
                <h6 className='card-text'>{listing.price}</h6>
               
              </div>
            </div>
          )
        })}
      
    </div>
    

      </div>  
  
    );
    
  }
  export default Home;