import { useState } from "react";
import axios from 'axios';
import {useNavigate} from 'react-router-dom' ;
const Signin=()=>{
       console.log('hello12345');
    const[email,setEmail]=useState('');
    const[password,setPassword]=useState('');
    
    const Navigate=useNavigate();
    const signin=()=>{
        if(email.length===0)
        alert('Plese enter email')
        else if(password.length===0)
        alert('please enter password')
        else{
         axios.post('http://localhost/user/signin',{
             email,
             password,
         }).then((response)=>{
            const result=response.data;
           
            if (result['status'] === 'notMatch') {
                alert('invalid email or password')
               
              } else {
                alert('welcome to CARAPI');
              
                Navigate('/home');

              }
         }).catch((error)=>{
             
            console.log('error')
            console.log(error)
         })

        }   
 }
  return (
    <div className="row">
        <div className="col"></div>
        <div className="col">
        <form>
    <div className="mb-3">
      <label htmlFor="exampleInputEmail1" className="form-label">Email address</label>
      <input onChange={(event)=>{
          setEmail(event.target.value)
      }} type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"/>
      <div id="emailHelp" className="form-text">We'll never share your email with anyone else.</div>
    </div>
    <div className="mb-3">
      <label htmlFor="exampleInputPassword1" className="form-label">Password</label>
      <input onChange={(event)=>{
          setPassword(event.target.value)
      }} type="password" className="form-control" id="exampleInputPassword1" />
    </div>
    <button onClick={signin} type="submit" className="btn btn-primary" style={{width:'100%'}}>Sign in</button>
  </form>
        </div>
        <div className="col"></div>
    </div>
  )
}
export default Signin;