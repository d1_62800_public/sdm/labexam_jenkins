import Signin from "./pages/signin";
import Home from "./pages/home";
import {BrowserRouter,Routes,Route} from 'react-router-dom'
import Navbar from "./components/navbar";
import AddCar from "./pages/addCar";
import Deletecar from "./pages/deletecar";

function App() {
  return (
    <BrowserRouter>
     <Navbar/>
    <Routes>
      <Route path="/signin" element={<Signin />}></Route>
      <Route path="/home" element={<Home />}></Route>
      <Route path="/addcar" element={<AddCar />}></Route>
      <Route path="/" element={<Home />}></Route>
      <Route path="/deletecar" element={<Deletecar />}></Route>

    
      
    </Routes>
    </BrowserRouter>
  );
}

export default App;
